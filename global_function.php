<?php

function getDirContents($dir)
{
  $handle = opendir($dir);
  if ( !$handle ) return array();
  $contents = array();
  while ( $entry = readdir($handle) )
  {
    if ( $entry=='.' || $entry=='..' || $entry=='.DS_Store' ) continue;

    $entry = $dir.DIRECTORY_SEPARATOR.$entry;
    if ( is_file($entry) )
    {
      $contents[] = $entry;
    }
    else if ( is_dir($entry) )
    {
      $contents = array_merge($contents, getDirContents($entry));
    }
  }
  closedir($handle);
  return $contents;
}

function truncateTableContent() {

	global $conn;

	$sql = "TRUNCATE table content_directories";
	if ($conn->query($sql) === TRUE) {
	    return true;
	} else {
	    echo "Error: " . $sql . "<br>" . $conn->error;
	    return false;
	}

}

function insertContent($content) {

	global $conn;

	$stmt = "SELECT * FROM content_directories where content like '$content'";
	$result = $conn->query($stmt);

	if ($result->num_rows > 0){
	    while($row = $result->fetch_assoc()) {
    		$counter = $row['counter'];
    		$id = $row['id'];
	    }
		$counter++;
		$sql = "UPDATE content_directories SET counter=".$counter." WHERE id=".$id;
		if ($conn->query($sql) === TRUE) {
		    return true;
		} else {
		    echo "Error: " . $sql . "<br>" . $conn->error;
		    return false;
		}
	}else{
		$sql = "INSERT INTO content_directories (content,counter) values ('$content',1)";
		if ($conn->query($sql) === TRUE) {
		    return true;
		} else {
		    echo "Error: " . $sql . "<br>" . $conn->error;
		    return false;
		}
	}

}

function getMaxContent(){
	global $conn;

	$stmt = "SELECT * FROM content_directories where counter = (select max(counter) from content_directories)";
	$result = $conn->query($stmt);

	if ($result->num_rows > 0) {	
	    return $result;
	} else {
	    return false;
	}

}
?>
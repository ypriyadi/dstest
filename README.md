# Introduction

Source code to get the most similar content of a list of files in a directory & its subdirectories

## Installation

### Set source path

Set the source path you want to scan on config.php

```bash
define("SOURCE_DIR","<your_path_here>");
```

### Database connection

Set the database parameters on config.php

```bash
define("DB_HOST","<database_host>");
define("DB_USERNAME","<database_username>");
define("DB_PASSWORD","<database_password>");
define("DB_NAME","<database_name>");
```

### Create tables

Execute tables.sql query to create necessary table

## Usage

Execute file scan_directory.php on your terminal
```python
php <your_app_directory>/scan_directory.php
```

Or setup & run on a local web server 
```python
http://<host>/scan_directory.php
```
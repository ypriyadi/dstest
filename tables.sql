--
-- Table structure for table `content_directories`
--

CREATE TABLE `content_directories` (
  `id` int(11) NOT NULL,
  `content` text NOT NULL,
  `counter` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for table `content_directories`
--
ALTER TABLE `content_directories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_content` (`content`(1000));

--
-- AUTO_INCREMENT for table `content_directories`
--
ALTER TABLE `content_directories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;
<?php

include_once __DIR__ . "/config.php";
include_once __DIR__ . "/connection.php";
include_once __DIR__ . "/global_function.php";

$dirs = getDirContents(SOURCE_DIR);

if(count($dirs)>0){
	truncateTableContent();

	foreach ($dirs as $dir) {
		$content = file_get_contents($dir);
		insertContent($content);
	}

	$max_content = getMaxContent();
	if($max_content){
		foreach ($max_content as $key => $value) {
			echo $value['content'].' '.$value['counter']."\n";;
		}
	}else{
		echo "data empty!";
	}
}else{
	echo "directory is empty!";
}

$conn->close();

exit;
?>